import React, { useEffect } from 'react';
import logo from './logo.svg';
import { Counter } from './features/counter/Counter';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import Login from './views/Login';
//import { selectAccount } from './features/account/accountSlice';
import Centers from './views/Centers';
import { useAppDispatch, useAppSelector } from './app/hooks';
//import Connection from './features/devices/DevicesHub';
import { CircularProgress, Grid } from '@mui/material';
import Devices from './views/Devices';
import AdminRequests from './views/Admin/Requests';
import HomeHub from './features/common/api/HomeHub';
//import { store } from './app/store';
//import { selectIsLoaded, selectIsLogged } from './features/account/accountSlice';
//import { load } from './features/account/accountSlice';
//import { enqueueSnackbarOverride } from './features/center/centerApi';
//import { enqueueSnackbar } from './features/snackbar/snackbarSlice';

function App() {
  //loadStore()
  //let account = store.getState().account // useAppSelector(selectAccount);
  //const dispatch = useAppDispatch();
  /*useEffect(()=>{
    dispatch(load())
  },[])*/
  //let { isLogged, isLoaded, isAdmin } = useAppSelector(selectAccount);
  let account = useAppSelector((state)=>state.account)
  let isHomeHubLoaded = useAppSelector((state)=>state.homeHub.isLoaded)
  //let isLoaded = useAppSelector(selectIsLoaded)
  if(!account.isLoaded){
    return <Loading/>
  }
  if(account.isLogged && !isHomeHubLoaded){
    return <HomeHub/>
  }
  
  return (
    <>
    <HomeHub/>
    <Router>
      <Switch>
        <Route path="/login" render={()=>!account.isLogged?<Login/>:<Redirect to="/"/>}></Route>
        <Route path="/centers" render={()=>account.isLogged?<Centers/>:<Redirect to="/login"/>}></Route>
        <Route path="/devices" render={()=>account.isLogged?<Devices/>:<Redirect to="/login"/>}></Route>
        <Route path="/admin/requests" render={()=>account.isLogged?<AdminRequests/>:<Redirect to="/"/>}></Route>
        <Route path="/" render={()=>account.isLogged?<Redirect to="/centers"/>:<Redirect to="/login"/>}></Route>
      </Switch>
    </Router>
    </>
  );
}

function Initial(){
  return <div className="App">
    <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <Counter />
      <p>
        Edit <code>src/App.tsx</code> and save to reload.
      </p>
      <span>
        <span>Learn </span>
        <a
          className="App-link"
          href="https://reactjs.org/"
          target="_blank"
          rel="noopener noreferrer"
        >
          React
        </a>
        <span>, </span>
        <a
          className="App-link"
          href="https://redux.js.org/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Redux
        </a>
        <span>, </span>
        <a
          className="App-link"
          href="https://redux-toolkit.js.org/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Redux Toolkit
        </a>
        ,<span> and </span>
        <a
          className="App-link"
          href="https://react-redux.js.org/"
          target="_blank"
          rel="noopener noreferrer"
        >
          React Redux
        </a>
      </span>
    </header>
  </div>
}
function Loading(){
  return <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justifyContent="center"
        style={{ minHeight: '100vh' }}
    >
        <CircularProgress />
    </Grid>
}
export default App;
