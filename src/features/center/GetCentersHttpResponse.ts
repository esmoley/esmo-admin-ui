import GetCentersVm from "./GetCentersVm";

export default interface GetCentersHttpResponse{
    status: number
    statusText: string
    data: null | GetCentersVm
}