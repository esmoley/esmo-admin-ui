import { RoleDto } from "./RoleDto";

export interface CenterDto{
    id:string
    title:string
    status:string
    roles:Array<RoleDto>
    isOwner:boolean
}