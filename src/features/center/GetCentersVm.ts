import { CenterDto } from "./CenterDto";

export default interface GetCentersVm{
    centers: Array<CenterDto>
}