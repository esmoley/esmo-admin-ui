import { useSnackbar } from "notistack";
import { SERVER_URL } from "../../app/consts";
import { Get } from "../common/api/api";
import { IHttpResult } from "../common/IHttpResult";
//import { enqueueSnackbar } from "../snackbar/snackbarSlice";
//import useNotifier from "../snackbar/useNotifier";
import GetCentersHttpResponse from "./GetCentersHttpResponse";

//export const enqueueSnackbarOverride = (message:any)=>{}

export default {
    getCentersAsync : async(): Promise<GetCentersHttpResponse>=>{
        //enqueueSnackbarOverride('Failed fetching data.')
        return await Get(SERVER_URL+"/api/Center/Get")
        /*let result:GetCentersVm = { status:0, statusText: '', data: null };
        let response = await fetch(SERVER_URL+"/api/Center/Get", {
            method:'GET',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json', 'authorization': 'Bearer ' + store.getState().account.token},
        });
        try{
            result.status = response.status;
            result.statusText = response.statusText;
            result.data = await response.json();
        }catch{}
        return result;*/
    },
}