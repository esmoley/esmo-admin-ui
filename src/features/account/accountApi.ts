import { SERVER_URL } from "../../app/consts";
import { IHttpResult } from "../common/IHttpResult";

export default {
    loginAsync : async(email:string, password:string, remember:boolean): Promise<IHttpResult>=>{
        let result:IHttpResult = { status:0, statusText: '', data: null as any};
        let response = await fetch(SERVER_URL+"/api/Account/Login", {
            method:'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ email: email, password: password, remember: remember })
        });
        try{
            result.status = response.status;
            result.statusText = response.statusText;
            result.data = await response.json();
        }catch{}
        return result;
    },
    autologinAsync : async(token:string): Promise<IHttpResult>=>{
        let result:IHttpResult = { status: 0, statusText: '', data: null as any };
        let response = await fetch(SERVER_URL+"/api/Account/AutoLogin", {
            method:'GET',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json', 'authorization': 'Bearer ' + token }
        });
        try{
            result.status = response.status;
            result.statusText = response.statusText;
            result.data = await response.json();
        }catch{}
        return result;
    }
}