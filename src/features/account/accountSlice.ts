import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { WritableDraft } from "immer/dist/internal";
//import { RootState } from "../../app/store";
import accountApi from "./accountApi";
import { showAlert } from "../common/api/api";
import { IHttpResult } from "../common/IHttpResult";
//import { RootState } from "../../app/store";

export interface AccountState {
    isLoaded: boolean,
    isLogged: boolean,
    userName: string,
    email: string,
    token: string,
    isAdmin: boolean,
    currentCenterId:string
}

const initialState: AccountState = {
    isLoaded: false,
    isLogged: false,
    userName: '',
    email: '',
    token: localStorage.getItem('token')||'',
    isAdmin: false,
    currentCenterId:''
}
export const loginAsync = createAsyncThunk(
    'account/login', async (payload:{ email:string; password:string; remember:boolean; }) => {
        let result = await accountApi.loginAsync(payload.email, payload.password, payload.remember);
        showAlert(result)
        return result;
    }
);
export const autologinAsync = createAsyncThunk(
    'account/autologin', async (payload:{ token:string }): Promise<IHttpResult> => {
        async function f(){
            try{
                return await accountApi.autologinAsync(payload.token);
            }catch{
                return await accountApi.autologinAsync(payload.token);
            }
        }
        return f();
    },
);

function logoutAction(state: WritableDraft<AccountState>){
    state.email = '';
    state.isLogged = false;
    state.userName = '';
    state.token = '';
    state.isAdmin = false;
    state.currentCenterId = '';
    localStorage.setItem("token","")
}
function setAccountAction(state: WritableDraft<AccountState>, action: { payload: any; type: string;}){
    if(action.payload.email)state.email = action.payload.email;
    if(action.payload.isLogged)state.isLogged = action.payload.isLogged;
    if(action.payload.userName)state.userName = action.payload.userName;
    if(action.payload.token)state.token = action.payload.token;
    if(action.payload.isAdmin)state.isAdmin = action.payload.isAdmin;
    if(action.payload.currentCenterId)state.currentCenterId = action.payload.currentCenterId;
}
function loadedAction(state: WritableDraft<AccountState>){
    console.log("loaded")
    state.isLoaded = true;
}

export const accountSlice = createSlice({
    name: 'account',
    initialState,
    reducers: {
        logout:(state)=>logoutAction(state),
        setAccount:(state, action)=>setAccountAction(state,action),
        loaded:(state)=>loadedAction(state)
        // #region s
        /*load:(state)=>{
            console.log("load")
            if(state.token != ""){
                const f = ()=>{
                    console.log("load2")
                    accountApi.autologinAsync(state.token).then(data=>{
                        switch(data.status){
                            case 401:
                                console.error("Not authorized!");
                                logoutAction(state)
                                break;
                            case 200:
                                console.error(data.data);
                                setAccountAction(state,{isLogged:true, ...data.data})
                        }
                        loadedAction(state)
                    }).catch(()=>{
                        setTimeout(f, 1000);
                    });
                }
                f();
            }else{
                loadedAction(state)
            }
        }*/
        //}
    // login: (state, action: PayloadAction<{ email:string; password:string; remember:boolean; }>)=>{
    //     accountApi.loginAsync(action.payload.email, action.payload.password, action.payload.remember).then((response)=>{
    //         state.token = response.accessToken;
    //         state.name = response.userName;
    //         state.email = response.email;
    //         state.isLogged = true;
    //         console.log(response);
    //     })
    // }
    // #endregion
    },
    extraReducers:(builder)=>{
        builder.addCase(loginAsync.fulfilled, (state, action) => {
            if(action.payload.status != 200){
                console.error(action.payload.statusText);
                return;
            }
            state.token = action.payload.data.accessToken;
            localStorage.setItem('token', action.payload.data.accessToken);
            state.userName = action.payload.data.userName;
            state.email = action.payload.data.email;
            state.isLogged = true;
            state.isAdmin = action.payload.data.isAdmin;
            state.currentCenterId = action.payload.data.currentCenterId;
            console.log(action);
        })
        .addCase(autologinAsync.fulfilled, (state, action)=>{
            if(action.payload.status != 200){
                console.error(action.payload.statusText);
                return;
            }
            state.userName = action.payload.data.userName;
            state.email = action.payload.data.email;
            state.isLogged = true;
            state.isAdmin = action.payload.data.isAdmin;
            state.currentCenterId = action.payload.data.currentCenterId;
            console.log(action);
        })
    }
})
export const { logout } = accountSlice.actions;
//export const selectAccount = (state: RootState) => state.account
//accountSlice.actions.logout();
/*
export const selectIsLoaded = (state: RootState) => state.account.isLoaded;
export const selectIsLogged = (state: RootState) => state.account.isLogged;
export const selectIsAdmin = (state: RootState) => state.account.isAdmin;
export const selectToken = (state: RootState) => state.account.token;
export const selectUserName = (state: RootState) => state.account.userName;
export const selectEmail = (state: RootState) => state.account.email;*/

//export const { login } = accountSlice.actions;

//export default accountSlice.reducer;