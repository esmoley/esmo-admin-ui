import { createSlice } from "@reduxjs/toolkit"
import { WritableDraft } from "immer/dist/internal"
import Cookies from 'universal-cookie';
const cookies = new Cookies();

export interface LanguageState {
    current:string
}

if(!cookies.get("language")){
    console.log("lang set")
    cookies.set("language",navigator.languages
    ? navigator.languages[0]
    : navigator.language)
}

const initialState: LanguageState = {
    current: cookies.get("language")
}
console.log(initialState.current)
export const languageSlice = createSlice({
    name: 'language',
    initialState,
    reducers: {
        setCurrent(state: WritableDraft<LanguageState>, action:{ payload:string }){
            cookies.set('language', state.current, { path: '/' });
            state.current = action.payload
        }
    }
})