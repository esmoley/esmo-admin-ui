import { IconButton, List, ListItem, ListItemIcon, ListItemText, ListSubheader, Menu, MenuItem } from "@mui/material";
import AccountCircle from '@mui/icons-material/AccountCircle';
import { useHistory } from 'react-router-dom';
import { logout } from "../account/accountSlice";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import React from "react";
import { useSelector } from "react-redux";

export default function UserMenu(){
    //const user = store.getState().account//useAppSelector(selectAccount)
    const account = useAppSelector((state)=>state.account)// useAppSelector(selectUserName)
    //const email = useAppSelector(selectEmail)
    const dispatch = useAppDispatch()
    let history = useHistory()
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleLogout = ()=>{
        dispatch(logout());
        handleClose();
    }
    const handleRedirectTo = (url:string)=>{
        history.push(url)
        handleClose();
    }
    const handleClose = () => {
        setAnchorEl(null);
    };
    
    return <>
        <IconButton
            size="large"
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleMenu}
            color="inherit"
        >
            <AccountCircle />
        </IconButton>
        <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
            }}
            keepMounted
            transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            open={Boolean(anchorEl)}
            onClose={handleClose}
        >
            <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}
            component="nav"
            aria-labelledby="nested-list-subheader"
            subheader={
                <ListSubheader component="div" id="nested-list-subheader">
                </ListSubheader>
            }
            >
                <ListItem alignItems="flex-start">
                    <ListItemIcon>
                        <AccountCircle />
                    </ListItemIcon>
                    <ListItemText primary={account.userName} secondary={account.email} />
                </ListItem>
            </List>
            <MenuItem onClick={()=>handleRedirectTo("/centers")}>Центры</MenuItem>
            <MenuItem onClick={handleLogout}>Выйти</MenuItem>
        </Menu>
    </>
}