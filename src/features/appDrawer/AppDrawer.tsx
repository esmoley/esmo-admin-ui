import { AppBar, Menu, MenuItem, Box, CssBaseline, Divider, IconButton, List, ListItem, ListItemIcon, ListItemText, Toolbar, Typography, Drawer, Grid, CircularProgress, ListItemAvatar, Avatar, ListSubheader } from "@mui/material";
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
import MenuIcon from '@mui/icons-material/Menu';
import AccountCircle from '@mui/icons-material/AccountCircle';
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { logout} from "../account/accountSlice";
import { useHistory } from 'react-router-dom';
import "@fontsource/cormorant-infant"
import React, { Children } from "react";
import UserMenu from "./UserMenu";
import AdminMenu from "./AdminMenu";
import { store } from "../../app/store";

const drawerWidth = 240;

interface IAppDrawerProps{
    title: string
    loaded: boolean
    children: React.ReactNode
}

function AppDrawer(props: IAppDrawerProps){
    const {title, children, loaded} = props;
    //const user = store.getState().account; // useAppSelector(selectAccount);
    const isAdmin = useAppSelector(state=>state.account.isAdmin)
    const dispatch = useAppDispatch();
    let history = useHistory()

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    
    const handleLogout = ()=>{
        dispatch(logout());
        handleClose();
    }
    const handleRedirectTo = (url:string)=>{
        history.push(url)
        handleClose();
    }
    const handleClose = () => {
        setAnchorEl(null);
    };

    const [mobileOpen, setMobileOpen] = React.useState(false);
    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };
    const drawer = (
    <div>
        <Toolbar>
            <Typography variant="h5" component="div" sx={{ flexGrow: 1 }} style={{fontFamily: "Cormorant Infant", fontWeight: 700}}>
                QSHELL
            </Typography>
        </Toolbar>
        <Divider />
        <List>
        {['Устройства'].map((text, index) => (
            <ListItem button key={text} onClick={()=>handleRedirectTo("/devices")}>
            <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
            <ListItemText primary={text} />
            </ListItem>
        ))}
        </List>
        <Divider />
        <List>
        {['All mail', 'Trash', 'Spam'].map((text, index) => (
            <ListItem button key={text}>
            <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
            <ListItemText primary={text} />
            </ListItem>
        ))}
        </List>
    </div>
    );
    const container = window !== undefined ? () => window.document.body : undefined;
    return (
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <AppBar
                position="fixed"
                sx={{
                width: { sm: `calc(100% - ${drawerWidth}px)` },
                ml: { sm: `${drawerWidth}px` },
                }}
                color='transparent' style={{backgroundColor:'white'}}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        sx={{ mr: 2, display: { sm: 'none' } }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap component="div" sx={{ flexGrow: 1 }}>
                        {title}
                    </Typography>
                    {isAdmin==true && <AdminMenu/>}
                    <UserMenu/>
                </Toolbar>
            </AppBar>
            <Box
                component="nav"
                sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
                aria-label="mailbox folders"
            >
                {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                <Drawer
                container={container}
                variant="temporary"
                open={mobileOpen}
                onClose={handleDrawerToggle}
                ModalProps={{
                    keepMounted: true, // Better open performance on mobile.
                }}
                sx={{
                    display: { xs: 'block', sm: 'none' },
                    '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                }}
                >
                {drawer}
                </Drawer>
                <Drawer
                variant="permanent"
                sx={{
                    display: { xs: 'none', sm: 'block' },
                    '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                }}
                open
                >
                {drawer}
                </Drawer>
            </Box>
            
            {loaded ? <>
            <Box
                component="main"
                sx={{ flexGrow: 1, p: 3, width: { sm: `calc(100% - ${drawerWidth}px)` } }}
            >
                <Toolbar />
                {children}
            </Box></> : <Loading/>}
        </Box>
    )
}
function Loading(){
    return <Grid
          container
          spacing={0}
          direction="column"
          alignItems="center"
          justifyContent="center"
          style={{ minHeight: '100vh' }}
      >
          <CircularProgress />
      </Grid>
}
export default AppDrawer;