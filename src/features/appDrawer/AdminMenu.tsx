import { Button, IconButton, List, ListItem, ListItemIcon, ListItemText, ListSubheader, Menu, MenuItem } from "@mui/material";
import AccountCircle from '@mui/icons-material/AccountCircle';
import { useHistory } from 'react-router-dom';
import { logout } from "../account/accountSlice";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import React from "react";

export default function AdminMenu(){
    const dispatch = useAppDispatch()
    let history = useHistory()
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleLogout = ()=>{
        dispatch(logout());
        handleClose();
    }
    const handleRedirectTo = (url:string)=>{
        history.push(url)
        handleClose();
    }
    const handleClose = () => {
        setAnchorEl(null);
    };
    
    return <>
        <Button
            size="large"
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleMenu}
            color="inherit"
        >
            Admin
        </Button>
        <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
            }}
            keepMounted
            transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            open={Boolean(anchorEl)}
            onClose={handleClose}
        >
            <MenuItem onClick={()=>handleRedirectTo("/admin/requests")}>Запросы</MenuItem>
        </Menu>
    </>
}