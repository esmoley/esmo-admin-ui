export interface CenterRequestDto{
    requestId:string
    title:string
    address:string
    phone:string
    site:string
    comment:string
    status:string
}