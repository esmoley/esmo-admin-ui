import { CenterRequestDto } from "./CenterRequestDto";
import { CentersRequestVm } from "./CentersRequestVm";

export default interface GetCentersRequestHttpResponse{
    status: number
    statusText: string
    data: null | CentersRequestVm
}