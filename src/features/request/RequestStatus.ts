export class RequestStatus{
    public value:string = ""
    public text:string = ""
    public color:string = ""

    public static create(value:string, text:string, color:string){
        let result = new RequestStatus();
        result.color = color;
        result.text = text;
        result.value = value;
        return result;
    }
    public static Accepted:RequestStatus = RequestStatus.create("Accepted","Принят","green")
    public static Pending:RequestStatus = RequestStatus.create("Pending","Ожидание решения администрации","orange")
    public static Denied:RequestStatus = RequestStatus.create("Denied","Отклонен","red")
    public static from(value:string):RequestStatus{
        switch(value){
            case this.Accepted.value:return this.Accepted;
            case this.Pending.value:return this.Pending;
            case this.Denied.value:return this.Denied;
        }
        return this.create("NotFound","Не найдено","gray")
    }
}