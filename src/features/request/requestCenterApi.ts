import { SERVER_URL } from "../../app/consts";
import GetCentersRequestHttpResponse from "./GetCentersRequestHttpResponse";
import { CenterRequestDto } from "./CenterRequestDto";
import { IHttpResult } from "../common/IHttpResult";
import { Get, Post } from "../common/api/api";

export default {
    createCenterAsync : async(title:string, address:string, phone:string, site:string, comment:string): Promise<IHttpResult>=>{
        return await Post(SERVER_URL+"/api/Request/CreateCenter",JSON.stringify({ title, address, phone, site, comment }))
        /*let result:IHttpResult = { status:0, statusText: '', data: null as any};
        let response = await fetch(SERVER_URL+"/api/Request/CreateCenter", {
            method:'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json', 'authorization': 'Bearer ' + store.getState().account.token},
            body: JSON.stringify({ title, address, phone, site, comment })
        });
        try{
            result.status = response.status;
            result.statusText = response.statusText;
            result.data = await response.json();
        }catch{}
        return result;*/
    },
    updateCreateCenterAsync : async(id:string, title:string, address:string, phone:string, site:string, comment:string): Promise<IHttpResult>=>{
        return await Post(SERVER_URL+"/api/Request/UpdateCreateCenter",JSON.stringify({ id, title, address, phone, site, comment }))
        /*let result:IHttpResult = { status:0, statusText: '', data: null as any};
        let response = await fetch(SERVER_URL+"/api/Request/UpdateCreateCenter", {
            method:'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json', 'authorization': 'Bearer ' + store.getState().account.token},
            body: JSON.stringify({ id, title, address, phone, site, comment })
        });
        try{
            result.status = response.status;
            result.statusText = response.statusText;
            result.data = await response.json();
        }catch{}
        return result;*/
    },
    getCentersAsync : async(): Promise<GetCentersRequestHttpResponse>=>{
        return await Get(SERVER_URL+"/api/Request/GetCreateCenterRequests")
        /*let result:GetCentersRequestVm = { status:0, statusText: '', data: null };
        let response = await fetch(SERVER_URL+"/api/Request/GetCreateCenterRequests", {
            method:'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json', 'authorization': 'Bearer ' + store.getState().account.token},
        });
        try{
            result.status = response.status;
            result.statusText = response.statusText;
            result.data = await response.json();
        }catch{}
        return result;*/
    }
}