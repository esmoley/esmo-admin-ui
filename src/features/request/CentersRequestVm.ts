import { CenterRequestDto } from "./CenterRequestDto";

export interface CentersRequestVm{
    requests: Array<CenterRequestDto>
}