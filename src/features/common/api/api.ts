import { useSnackbar } from "notistack";
//import { store } from "../../app/store";
import { IHttpResult } from "../IHttpResult";
import SnackbarUtils from "../../snackbar/SnackbarUtils";

export const showAlert = function showAlert(result:IHttpResult){
    if(result.status == 200 && (result.statusText == null || result.statusText == ""))return;

    if(result.statusText == null || result.statusText == ""){
        result.statusText = createTextFromHttpStatus(result.status)
    }
    if(result.status>=400){
        SnackbarUtils.error(result.statusText)
    }else if(result.status>=300){
        SnackbarUtils.warning(result.statusText)
    }
}

function createTextFromHttpStatus(status:number):string{
    switch(status){
        case 200: return "Успешно"
        case 500: return "Ошибка сервера"
        default:
            return "Статус " + status
    }
}
function getToken(){
    return localStorage.getItem('token')
}
export const Post = async function Post(url: string, body?:string) : Promise<IHttpResult>{
    let result:IHttpResult = { status:0, statusText: '', data: null }
    let response = await fetch(url, {
        method:'POST',
        credentials: 'include',
        headers: { 'Content-Type': 'application/json', 'authorization': 'Bearer ' + getToken()},
        body: body
    })
    //let result:any = {status:200, statusText:''};
    try{
        result.status = response.status
        result.statusText = response.statusText
        result.data = response.json()
    }catch(ex){
        //result.status = 0
        result.statusText = ex as string;
    }
    showAlert(result)

    return result;
}
export const Get = async function Get(url: string) : Promise<IHttpResult>{
    //const {enqueueSnackbar} = useSnackbar()
    //enqueueSnackbar("asd")
    let result:IHttpResult = { status:0, statusText: '', data: null }
    let response = await fetch(url, {
        method:'GET',
        credentials: 'include',
        headers: { 'Content-Type': 'application/json', 'authorization': 'Bearer ' + getToken()}
    })
    //let result:any = {status:200, statusText:''};
    try{
        result.status = response.status
        result.statusText = response.statusText
        result.data = await response.json()
    }catch(ex){
        //result.status = 0
        result.statusText = ex as string;
    }
    showAlert(result)

    return result;
}