export default class HomeHubSendMethod{
    private constructor(public name:string){}
    
    static GetCreateCenterRequests = new HomeHubSendMethod("GetCreateCenterRequests");
    static GetCenters = new HomeHubSendMethod("GetCenters");
    static AddCreateCenterRequest = new HomeHubSendMethod("AddCreateCenterRequest")
    static UpdateCreateCenterRequest = new HomeHubSendMethod("UpdateCreateCenterRequest")
    static GetAdminRequests = new HomeHubSendMethod("GetAdminRequests")
    static ProcessAdminRequests = new HomeHubSendMethod("ProcessAdminRequests")
    static SetCurrentCenter = new HomeHubSendMethod("SetCurrentCenter")
}