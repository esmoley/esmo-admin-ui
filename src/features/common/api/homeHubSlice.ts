import { createSlice, PayloadAction } from "@reduxjs/toolkit"

export interface HomeHubState {
    isLoaded: boolean
}
const initialState:HomeHubState = {
    isLoaded: false
}
export const homeHubSlice = createSlice({
    name:"homeHub",
    initialState,
    reducers:{
        setLoaded:(state, action: PayloadAction<boolean>)=>{state.isLoaded = action.payload}
    }
})
export const {setLoaded} = homeHubSlice.actions