import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { loginAsync } from "../../account/accountSlice";
import { HubConnection, HubConnectionBuilder, HubConnectionState } from "@microsoft/signalr";
import { HOME_HUB } from "../../../app/consts";
import HomeHubReceiveMethod from "./HomeHubReceiveMethod";
import HomeHubSendMethod from "./HomeHubSendMethod";
import { setLoaded } from "./homeHubSlice";
import { useEffect } from "react";
import SnackbarUtils from "../../snackbar/SnackbarUtils";
//import { store } from "../../app/store";

let connection: HubConnection|null = null;
let callbacksMap = new Map<string, Array<(payload:any)=>any>>();

interface ConnectRequest{
    token:string
    reconnectCounter:number
    onConnected:()=>void
    lang:string
}

export const showWsAlert = function showWsAlert(result:any){
    if(result==null)return;
    if(result.error != null && result.error != ""){
        SnackbarUtils.error(result.error)
    }
}

export const homeHubSubscribe = (method: HomeHubReceiveMethod, callback: (payload: any) => void)=> {
    let found = callbacksMap.get(method.name);
    if(found!=undefined)found.push(callback);
    else callbacksMap.set(method.name,[callback]);
}

export const homeHubSend = (methodName: HomeHubSendMethod, ...args: any[])=>{
    let result = connection?.invoke(methodName.name, ...args)
    result?.then(res=>showWsAlert(res))
    return result;
}

const notify = (method:HomeHubReceiveMethod, payload:any)=>{
    console.log("notify1: "+ method + " " + payload)
    let found = callbacksMap.get(method.name)
    if(found==undefined)return;
    found.forEach(cb=>cb(payload));
}
let reconnectingAction = (request:ConnectRequest)=>{}
export function Disconnect(){
    reconnectingAction = (request:ConnectRequest)=>{
        if(connection!=null){
            connection.stop()
            connection = null;
        }
        //Connect(request)
    }
    connection?.stop()
}
function Connect(request:ConnectRequest){
    try{
        connection = new HubConnectionBuilder()
            .withUrl(HOME_HUB+`?lang=${request.lang}` //+ `?access_token=${request.token}` 
            , { accessTokenFactory: () => request.token }
            )
            .build();
        HomeHubReceiveMethod.GetAll().forEach(x=>{
            connection?.on(x.name, (data: any) => {
                notify(x, data);
            });
        })
        /*connection.on("Receive", (data: any) => {
            console.log(data);
        });*/
        connection.onclose((ex) => {
            console.log('closed '+ex)
            if(request.reconnectCounter>10){
                console.error("Too many reconnecting attempts..")
                window.location.reload();
                return;
            }else{
                reconnectingAction(request)
            }
        })
        connection.start().then(() => {
            request.onConnected()
            request.reconnectCounter=0;
            connection?.send("send");
        }).catch((ex) => {
            console.log('exception ' + request.reconnectCounter)
            request.reconnectCounter++;
            console.error(ex);
            if(request.reconnectCounter>10){
                console.error("Too many reconnecting attempts..")
                window.location.reload();
                return;
            }else{
                reconnectingAction(request)
            }
        });
    }catch(exc){
        console.log("exc"+ exc)
    }
}
function Connection() {
    let account = useAppSelector((state)=>state.account)
    let homeHub = useAppSelector((state)=>state.homeHub)
    let language = useAppSelector((state)=>state.language)
    const dispatch = useAppDispatch();
    //const account = store.getState().account //useAppSelector(selectAccount);
    //const isLoaded = useAppSelector(selectIsLoaded)
    //const isLogged = useAppSelector(selectIsLogged)
    //const token = useAppSelector(selectToken)
    if (!account.isLoaded) return <></>
    if (account.isLogged) {
        reconnectingAction = (request:ConnectRequest)=>{setTimeout(()=>{
                if(connection!=null){
                    connection.stop()
                }
                Connect(request)
            }, request.reconnectCounter*1000+500)
        }
        if (connection == null) {
            Connect({
                    token: account.token,
                    reconnectCounter: 0,
                    onConnected: ()=>{
                        if(!homeHub.isLoaded){
                            dispatch(setLoaded(true))
                        }
                    },
                    lang: language.current
                } as ConnectRequest)
            }
    } else {
        reconnectingAction = (request:ConnectRequest)=>{}
        if(homeHub.isLoaded){
            dispatch(setLoaded(false))
        }
        if (connection != null) {
            connection.stop();
            connection = null;
        }
    }
    return <></>
}
export default Connection;