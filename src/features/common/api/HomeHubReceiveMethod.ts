import GetCentersRequestHttpResponse from "../../request/GetCentersRequestHttpResponse";

export default class HomeHubReceiveMethod{
    private constructor(public name:string){}

    static GetCreateCenterRequests = new HomeHubReceiveMethod("GetCreateCenterRequests");

    public static GetAll():Array<HomeHubReceiveMethod>{
        return [
            this.GetCreateCenterRequests,
        ]
    }
}