export interface IHttpResult{
    status:number;
    statusText:string;
    data:any;
}