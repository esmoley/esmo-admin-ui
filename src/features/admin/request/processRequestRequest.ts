export class ProcessRequestRequest{
    public id:string=""
    public data:string = ""
    public status:string = ""
    public statusReason:string = ""
}