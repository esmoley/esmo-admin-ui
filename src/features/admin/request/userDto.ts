export interface UserDto{
    createdAt: Date
    currentCenterId: null|string
    email: string
    id: string
    isAdmin: boolean
    phoneNumber: string
    updatedAt: string
    userName: string
}