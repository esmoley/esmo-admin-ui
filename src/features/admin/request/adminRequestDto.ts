import { UserDto } from "./userDto";

export class AdminRequestDto {
    public id:string=""
    public data:string = ""
    public user:null|UserDto = null
    public requestedByUserId:null|string = null
    public type:string = ""
    public status:string = ""
    public processedByAdminId:string = ""
    public createdAt:Date = new Date()
    public updatedAt:Date = new Date()
    public statusReason:string = ""
    /* public GetDataMap(): Map<string, string>{
        let result = new Map<string, string>();
        let parsed = JSON.parse(this.data);
        for (const k in parsed) {
            result.set(k, parsed[k]);
        }
        return result;
    } */
}
