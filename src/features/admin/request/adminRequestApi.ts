import { SERVER_URL } from "../../../app/consts";
import { Get, Post } from "../../common/api/api";
import { IHttpResult } from "../../common/IHttpResult";
import { AdminRequestApiVm } from "./adminRequestApiVm";
import { ProcessRequestRequest } from "./processRequestRequest";
//import GetCentersRequestVm from "./GetCentersRequestVm";
//import { CenterRequestDto } from "./CenterRequestDto";
export default {
    getRequestsAsync : async(): Promise<AdminRequestApiVm>=>{
        var response = await Get(SERVER_URL+"/api/Admin/Request/Get");
        return response
        /*let result:AdminRequestApiVm = { status:0, statusText: '', data: null };
        let response = await fetch(SERVER_URL+"/api/Admin/Request/Get", {
            method:'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json', 'authorization': 'Bearer ' + store.getState().account.token},
        });
        try{
            result.status = response.status;
            result.statusText = response.statusText;
            result.data = await response.json();
        }catch{}
        return result;*/
    },
    processRequestAsync : async(request: ProcessRequestRequest): Promise<IHttpResult>=>{
        return await Post(SERVER_URL+"/api/Admin/Request/Process", JSON.stringify(request))
        /*let result:IHttpResult = { status:0, statusText: '', data: null as any};
        let response = await fetch(SERVER_URL+"/api/Admin/Request/Process", {
            method:'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json', 'authorization': 'Bearer ' + store.getState().account.token},
            body: JSON.stringify(request)
        });
        try{
            result.status = response.status;
            result.statusText = response.statusText;
            result.data = await response.json();
        }catch{}
        return result;*/
    }
}