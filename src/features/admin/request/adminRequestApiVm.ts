import { AdminRequestDto } from "./adminRequestDto";
import GetAdminRequestsVm from "./GetAdminRequestsVm";

export interface AdminRequestApiVm{
    status: number
    statusText: string
    data: null | GetAdminRequestsVm
}