import { AdminRequestDto } from "./adminRequestDto";

export default interface GetAdminRequestsVm{
    requests: Array<AdminRequestDto>
}