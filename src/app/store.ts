import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
//import accountReducer from '../features/account/accountSlice';
import {accountSlice} from '../features/account/accountSlice';
import accountApi from "../features/account/accountApi";
import { homeHubSlice } from '../features/common/api/homeHubSlice';
import { languageSlice } from '../features/language/languageSlice';

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    account: accountSlice.reducer,
    homeHub: homeHubSlice.reducer,
    language: languageSlice.reducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
//let loaded:boolean = false;
//let v = store.getState().account

//export const loadStore = ()=>{
  //if(loaded) return
  //loaded=true
  if(store.getState().account.token != ""){
    const f = ()=>{
      accountApi.autologinAsync(store.getState().account.token).then(data=>{
          switch(data.status){
              case 401:
                console.error("Not authorized!");
                store.dispatch(accountSlice.actions.logout());
                break;
              case 200:
                console.error(data.data);
                store.dispatch(accountSlice.actions.setAccount({isLogged:true, ...data.data}));
          }
          store.dispatch(accountSlice.actions.loaded());
      }).catch(()=>{
        setTimeout(f, 1000);
      });
    }
    f();
  }else{
    store.dispatch(accountSlice.actions.loaded());
  }
//}