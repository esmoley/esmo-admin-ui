import { HubConnection, HubConnectionBuilder } from "@microsoft/signalr";
import { HOME_HUB } from "./consts";

export const sockets = new class{
    connection:HubConnection|null = null;
    constructor(){}
    Init(token:string){
        console.log("initiated");
        //var token = "1eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjMiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdCIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6InVzZXIiLCJuYmYiOjE2MzYzMDA5NzgsImV4cCI6MTY2NzgzNjk3OCwiaXNzIjoiRXNtb1NlcnZlciIsImF1ZCI6IkVzbW9DbGllbnQifQ.Us9IEfVddJpZpz83Pu1Nc2nPzpauyoNOy2VcdrZEu54";
        this.connection = new HubConnectionBuilder()
        .withUrl(HOME_HUB, { accessTokenFactory: () => token})
        .build();
        this.connection.on("Receive", (data:any) => {
            console.log(data);
        });
        this.connection.start().then(()=>{
            this.connection?.send("send");
        }).catch((ex)=>{
            console.log(ex);
        });
    }
}