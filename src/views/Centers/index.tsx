import { Box, Button, Card, CardActions, CardContent, Chip, CircularProgress, Grid, LinearProgress, Typography } from "@mui/material";
import React, { useEffect } from "react";
import { homeHubSend, homeHubSubscribe } from "../../features/common/api/HomeHub";
import HomeHubReceiveMethod from "../../features/common/api/HomeHubReceiveMethod";
import HomeHubSendMethod from "../../features/common/api/HomeHubSendMethod";
import AppDrawer from "../../features/appDrawer/AppDrawer";
import centerApi from "../../features/center/centerApi";
import { CenterDto } from "../../features/center/CenterDto";
import GetCentersVm from "../../features/center/GetCentersVm";
import { CenterRequestDto } from "../../features/request/CenterRequestDto";
import { CentersRequestVm } from "../../features/request/CentersRequestVm";
import requestApi from "../../features/request/requestCenterApi";
import SnackbarUtils from "../../features/snackbar/SnackbarUtils";
//import { enqueueSnackbar } from "../../features/snackbar/snackbarSlice";
import EditRequestCreateCenterDialog from "./EditRequestCreateCenterDialog";
import RequestCreateCenterDialog from "./RequestCreateCenterDialog";
import { useAppSelector } from "../../app/hooks";
import CenterChooseButton from "./CenterChooseButton";
import CenterCurrentButton from "./CenterCurrentButton";
//import { useSnackbar } from 'notistack';
let subscribed = false;
function Centers(){
    //const { enqueueSnackbar, closeSnackbar } = useSnackbar();
    const [createDialogOpen, setCreateDialogOpen] = React.useState(false);
    const [editDialogOpen, setEditDialogOpen] = React.useState(false);
    const [editDialogCenterRequest, setEditDialogCenterRequest] = React.useState({} as CenterRequestDto)
    const currentCenterId = useAppSelector(state=>state.account.currentCenterId)
    /*let editDialogCenterRequest = {} as CenterRequestDto;
    const setEditDialogCenterRequest = (centerRequestDto: CenterRequestDto)=>{
        editDialogCenterRequest = centerRequestDto;
    }*/

    const [requestsLoaded, setRequestsLoaded] = React.useState(false);
    const [centersLoaded, setCentersLoaded] = React.useState(false);
    
    const [createCenterRequests, setCreateCenterRequests] = React.useState(Array<CenterRequestDto>());
    const [centers, setCenters] = React.useState(Array<CenterDto>());

    useEffect(()=>{
        if(subscribed)return;
            /*homeHubSubscribe(HomeHubReceiveMethod.GetCreateCenterRequests, (payload:Array<CenterRequestDto>)=>{
                console.log(HomeHubReceiveMethod.GetCreateCenterRequests.name)
                console.log(payload)
            })*/
        subscribed = true;
    },[])
    useEffect (() => {
        if(!requestsLoaded){
            homeHubSend(HomeHubSendMethod.GetCreateCenterRequests)?.then((res: CentersRequestVm)=>{
                if(res==null || res.requests == null){
                    setCreateCenterRequests(Array<CenterRequestDto>())
                }else{
                    setCreateCenterRequests(res.requests)
                }
                setRequestsLoaded(true)
            })
            /*requestApi.getCentersAsync().then(response=>{
                if(response.data==null || response.data.requests == null){
                    setCreateCenterRequests(Array<CenterRequestDto>())
                }else{
                    setCreateCenterRequests(response.data.requests)
                }
                setRequestsLoaded(true)
                //enqueueSnackbar('I love hooks');
            })*/
        }
    },[requestsLoaded])

    useEffect (() => {
        if(!centersLoaded){
            homeHubSend(HomeHubSendMethod.GetCenters)?.then((res: GetCentersVm)=>{
                if(res==null || res.centers == null){
                    setCenters(Array<CenterDto>())
                }else{
                    setCenters(res.centers)
                }
                setCentersLoaded(true)
            })
            /*centerApi.getCentersAsync().then(response=>{
                setCenters(response.data!=null?response.data:Array<CenterDto>())
                setCentersLoaded(true)
            })*/
        }
    },[centersLoaded])
    

    let requestsContent = <></>
    if(createCenterRequests.length>0){
        let reqJsx = createCenterRequests.map((req, index)=>
            <Grid item key={index}>
                <Card sx={{ minWidth: 275 }} key={index}>
                    <CardContent>
                        {/*<Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                            Заявка на добавление центра
                        </Typography>*/}
                        <Typography sx={{ mb: 1.5 }} color="text.secondary">
                            {req.requestId}
                        </Typography>
                        <Typography variant="h6">
                            {req.title}
                        </Typography>
                        {req.status == "Pending" && <Typography color={"orange"}>Ожидание решения администрации</Typography>}
                        {req.status == "Denied" && <Typography color={"red"}>Отклонен</Typography>}
                    </CardContent>
                    <CardActions>
                        <Button size="small" onClick={()=>{
                            setEditDialogCenterRequest(req)
                            setEditDialogOpen(true)
                        }}>Редактировать</Button>
                    </CardActions>
                </Card>
            </Grid>
        )
        requestsContent = <Grid item pb={2}><Typography variant="h5" pb={2}>Заявки</Typography><Grid container spacing={2}>{reqJsx}</Grid></Grid>
    }
    let centersContent = <></>
    if(centers.length>0){
        let reqJsx = centers.map((req, index)=>
        {
            //let isOwner = req.isOwner;
            let isCurrent = currentCenterId == req.id;
            let chooseActionButton = isCurrent? <CenterCurrentButton/> : <CenterChooseButton id={req.id}/>
            return <Grid item key={index}>
                <Card sx={{ minWidth: 275 }} key={index}>
                    <CardContent>
                        {/*<Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                            Центр
    </Typography>*/}
                        <Typography sx={{ mb: 1.5 }} color="text.secondary">
                            {req.id}
                        </Typography>
                        <Typography variant="h6">
                            {req.title}
                        </Typography>
                        {req.status!= "OK" &&
                        <Typography variant="h6">
                            {req.status}
                        </Typography>
                        }
                        
                        {/*<Typography variant="subtitle1">
                                Владелец
                        </Typography>*/}
                    
                        {req.roles.map((req1, index1)=>
                            <Typography variant="subtitle1" key={index1}>
                                {req1.title}
                            </Typography>
                        )}
                        {req.isOwner && 
                            <Chip label="Владелец" variant="outlined" size="small" />
                        }
                    </CardContent>
                    <CardActions>
                        {chooseActionButton}
                    </CardActions>
                </Card>
            </Grid>
        }
        )
        centersContent = <Grid item><Typography variant="h5" pb={2}>Центры</Typography><Grid container spacing={2}>{reqJsx}</Grid></Grid>
    }
    let content = <Grid container spacing={1} justifyContent={"space-between"}>
        { /*
        <Grid item xs={12}>
            <Typography variant="h3" component="h3">
                Центры
            </Typography>
        </Grid>
        */ }
        <Grid item xs={12} style={{ display: "flex" }}>
            <Button variant="contained" style={{ marginLeft: "auto" }} onClick={()=>setCreateDialogOpen(true)}>
                Создать
            </Button>
        </Grid>
        {requestsContent}
        {centersContent}
        {(createCenterRequests.length == 0 && centers.length == 0) &&
            <Grid item xs={12}>
                <Typography variant="body1" component="p" fontStyle={{color:"grey"}}>
                    Создайте заявку на добавление центра, чтобы начать
                </Typography>
            </Grid>
        }
    </Grid>
    return <>
        <AppDrawer title="Центры" loaded={requestsLoaded && centersLoaded}>
            {content}
        </AppDrawer>
        <RequestCreateCenterDialog isOpen={createDialogOpen} setOpen={setCreateDialogOpen} update={()=>setRequestsLoaded(false)} />
        <EditRequestCreateCenterDialog isOpen={editDialogOpen} setOpen={setEditDialogOpen} update={()=>setRequestsLoaded(false)} centerRequest={editDialogCenterRequest} />
    </>
}
export default Centers;