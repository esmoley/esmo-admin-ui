import { Button, Grid, Typography } from "@mui/material";
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';

function CenterCurrentButton(){
    return <>
    <Grid container direction="row" alignItems="center" color="success.main">
        <Grid item pr={1}>
            <CheckCircleOutlineIcon fontSize="small"/>
        </Grid>
        <Grid item>
            <Typography variant="button" display="block" gutterBottom>Текущий</Typography>
        </Grid>
    </Grid>
    </>
    //return <Button color="success" startIcon={<CheckCircleOutlineIcon/>}>Текущий</Button>
    
    return <div style={{
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap',
    }}>
        <CheckCircleOutlineIcon />
        <span>Текущий</span>
    </div>  
    return <><CheckCircleOutlineIcon/>
        <Typography color="success" variant="button" display="block" gutterBottom>Текущий</Typography>
        </>
}
export default CenterCurrentButton;