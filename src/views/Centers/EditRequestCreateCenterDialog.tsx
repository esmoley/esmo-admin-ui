import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import requestApi from '../../features/request/requestCenterApi'
import { CenterRequestDto } from '../../features/request/CenterRequestDto';
import { useEffect } from 'react';
import HomeHubSendMethod from '../../features/common/api/HomeHubSendMethod';
import { homeHubSend } from '../../features/common/api/HomeHub';

interface EditRequestCreateCenterDialogProps{
  isOpen:boolean
  setOpen(open:boolean):void
  update():void
  centerRequest:CenterRequestDto
}
export default function EditRequestCreateCenterDialog(props: EditRequestCreateCenterDialogProps) {
    const {isOpen, setOpen, centerRequest, update} = props

    const requestId = centerRequest.requestId
    const theme = useTheme()
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'))
    const [disableActions, setDisableActions] = React.useState(false)
    
    const [title, setTitle] = React.useState("")
    const [titleError, setTitleError] = React.useState("")

    const [address, setAddress] = React.useState("")
    const [addressError, setAddressError] = React.useState("")

    const [phone, setPhone] = React.useState("")//centerRequest.phone);
    const [phoneError, setPhoneError] = React.useState("")

    const [site, setSite] = React.useState("")
    const [siteError, setSiteError] = React.useState("")

    const [comment, setComment] = React.useState("")

    useEffect (() => {
      if(isOpen){
        setDisableActions(false)
        setTitle(centerRequest.title)
        setTitleError("")
        setAddress(centerRequest.address)
        setAddressError("")
        setPhone(centerRequest.phone)
        setPhoneError("")
        setSite(centerRequest.site)
        setSiteError("")
        setComment(centerRequest.comment)
      }
    },[isOpen])
    
    const handleClose = () => {
      setOpen(false);
    };
    const handleSave = () =>{
      if(!validate())return;
      setDisableActions(true)
      homeHubSend(HomeHubSendMethod.UpdateCreateCenterRequest,{ id:requestId, title, address, phone, site, comment })?.then((response:any)=>{
        if(response?.error!=null)return;
        handleClose();
        update()
      }).finally(()=>{
        setDisableActions(false)
      })
      /*
      requestApi.updateCreateCenterAsync(requestId, title, address, phone, site, comment).then(()=>{
        update()
        handleClose()
      }).catch((error)=>{
        alert(error)
      }).finally(()=>{setDisableActions(false)});*/
    }

    const validate = () =>{
        return validateTitle() && validateAddress() && validatePhone() && validateSite();
    }
    const validateTitle = ()=>{
      if(title == "")setTitleError("Поле не может быть пустым")
      return titleError=="";
    }
    const validateAddress = ()=>{
      if(address == "")setAddressError("Поле не может быть пустым")
      return addressError=="";
    }
    const validatePhone = ()=>{
      if(phone == "")setPhoneError("Поле не может быть пустым")
      return phoneError=="";
    }
    const validateSite = ()=>{
      if(site == "")setSiteError("Поле не может быть пустым")
      return siteError=="";
    }
  
    return (
      <div>
        <Dialog open={isOpen} onClose={handleClose} fullScreen={fullScreen}>
          <DialogTitle>Редактирование заявки на добавление центра</DialogTitle>
          <DialogContent>
            <DialogContentText>
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="title"
              label="Название"
              type="text"
              fullWidth
              variant="standard"
              value={title}
              onChange={(e)=>{setTitleError(""); setTitle(e.target.value)}}
              error={titleError!=""}
              helperText={titleError}
              onBlur={validateTitle}
            />
            <TextField
              margin="dense"
              id="address"
              label="Физический адрес"
              type="text"
              fullWidth
              variant="standard"
              value={address}
              error={addressError!=""}
              helperText={addressError}
              onChange={(e)=>{setAddressError(""); setAddress(e.target.value)}}
              onBlur={validateAddress}
            />
            <TextField
              margin="dense"
              id="phone"
              label="Телефон"
              type="tel"
              fullWidth
              variant="standard"
              value={phone}
              error={phoneError!=""}
              helperText={phoneError}
              onChange={(e)=>{setPhoneError(""); setPhone(e.target.value)}}
              onBlur={validatePhone}
            />
            <TextField
              margin="dense"
              id="site"
              label="Ссылка на сайт или группу"
              type="url"
              fullWidth
              variant="standard"
              value={site}
              error={siteError!=""}
              helperText={siteError}
              onChange={(e)=>{setSiteError(""); setSite(e.target.value)}}
              onBlur={validateSite}
            />
            <TextField
              margin="dense"
              id="comment"
              label="Комментарий"
              type="text"
              fullWidth
              variant="standard"
              value={comment}
              onChange={(e)=>{setComment(e.target.value)}}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} disabled={disableActions}>Отменить</Button>
            <Button onClick={handleSave} disabled={disableActions}>Сохранить</Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }

function setDisableActions(arg0: boolean) {
  throw new Error('Function not implemented.');
}
