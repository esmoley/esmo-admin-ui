import { Button } from "@mui/material";
import HomeHubSendMethod from '../../features/common/api/HomeHubSendMethod';
import { Disconnect, homeHubSend } from '../../features/common/api/HomeHub';
import React from "react";
interface CenterChooseButtonProps{
    id:string
}
function CenterChooseButton(props: CenterChooseButtonProps){
    const [disableActions, setDisableActions] = React.useState(false)
    let ChooseCenter = ()=>{
        setDisableActions(true)
        homeHubSend(HomeHubSendMethod.SetCurrentCenter,{ id:props.id })?.then((response:any)=>{
            if(response?.error!=null)return;
            //Disconnect();
            window.location.reload();
        }).catch((ex=>{
            console.error("HomeHubSendMethod.SetCurrentCenter "+HomeHubSendMethod.SetCurrentCenter)
        })).finally(()=>{
            setDisableActions(false)
        })
    }
    
    return <Button size="small" onClick={ChooseCenter}>Выбрать</Button>
}
export default CenterChooseButton;