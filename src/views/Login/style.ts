import { grey } from "@mui/material/colors";
import React from "react";

export default {
    paper: {
        padding: 16,
        //margin:60,
        //marginTop: 16, // theme.spacing(12),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
         //theme.spacing(2),
        background: grey[900],
    } as React.CSSProperties,
    title: {
        fontFamily: 'Open Sans Condensed, sans-serif',
        fontWeight:600,
        fontSize:'2.2rem',
        lineHeight:'2'
    } as React.CSSProperties,
}