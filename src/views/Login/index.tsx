import { Box, Button, Checkbox, Container, FormControlLabel, Grid, Link, Paper, TextField, Typography, useMediaQuery } from "@mui/material";
import React, { ChangeEvent } from "react";
import { connect } from "react-redux";
import { useAppSelector, useAppDispatch } from "../../app/hooks";
import { loginAsync } from "../../features/account/accountSlice";
import style from "./style";

const styles = {
    box:{
        height:'100vh',
        display: "flex",
        flexDirection: "column",
        justifyContent: "center"
    }
}

class Login extends React.Component<any>{
    constructor(props:any){
        super(props);
        this.setState({
            email:'',
            password:'',
            remember:false
        })
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    state = {
        email:'',
        password:'',
        remember:false,
    }
    handleInputChange(event:React.ChangeEvent<HTMLInputElement>) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
          [name]: value
        });
    }
    handleSubmit(event: React.FormEvent<HTMLFormElement>){
        event.preventDefault();
        this.props.dispatch(loginAsync({ email: this.state.email, password: this.state.password, remember: this.state.remember}));
    }
    render(){
        var isMobile = this.props.isMobile;
        return <>
            <Container fixed style={{
                height:'100vh',
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                minWidth:'350px',
                width:isMobile?'100vw':'auto',
                maxWidth:isMobile?'fit-content':'500px',
            }} disableGutters>
                <Paper elevation={3} style={{
                    padding:'20px', 
                    height:isMobile?'100vh':'auto',
                    display:'grid',
                    alignItems:"center"
                    }}>
                    <div>
                        <Typography component="h1" variant="h5">
                            {process.env.REACT_APP_NAME}ESMO
                        </Typography>
                        <form noValidate onSubmit={this.handleSubmit}>
                            <TextField
                                margin="normal"
                                fullWidth
                                id="email"
                                label="Email"
                                name="email"
                                variant="outlined"
                                autoComplete="email"
                                onChange={this.handleInputChange}
                                autoFocus
                            />
                            <TextField
                                margin="normal"
                                fullWidth
                                name="password"
                                label="Пароль"
                                type="password"
                                variant="outlined"
                                id="password"
                                onChange={this.handleInputChange}
                                autoComplete="current-password"
                            />
                            <FormControlLabel
                                control={<Checkbox name="remember" value="remember" color="primary" onChange={this.handleInputChange} />}
                                label="Запомнить"
                            />
                            <Button
                                fullWidth
                                type="submit"
                                variant="contained"
                                size= "large"
                                color="primary"
                            >
                                Войти
                            </Button>
                            
                            <Grid container>
                                <Grid item xs>
                                <Link href="#" variant="body2" style={{textDecoration: "line-through"}} >
                                    Забыли пароль?
                                </Link>
                                </Grid>
                                <Grid item>
                                <Link href="#" variant="body2" style={{textDecoration: "line-through"}}>
                                    {"Регистрация"}
                                </Link>
                                </Grid>
                            </Grid>
                        </form>
                    </div>
                </Paper>
            </Container>
        </>
    }
}
function Wrapper(props:any){
    const mediaSmUp = useMediaQuery('(min-width:600px)');
    const dispatch = useAppDispatch();
    //const account = store.getState().account //useAppSelector(selectAccount);
    //var matches = useMediaQuery(theme => theme.breakpoints.up('sm'));
    return <Login {...props} isMobile={!mediaSmUp} dispatch={dispatch} />
}
export default Wrapper;

