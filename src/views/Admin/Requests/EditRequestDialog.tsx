import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useEffect } from 'react';
import { AdminRequestDto } from '../../../features/admin/request/adminRequestDto';
import { IconButton, Tooltip, Typography } from '@mui/material';
import adminRequestApi from '../../../features/admin/request/adminRequestApi';
import { ProcessRequestRequest } from '../../../features/admin/request/processRequestRequest';
import CloseIcon from '@mui/icons-material/Close';
import SaveIcon from '@mui/icons-material/Save';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import { RequestStatus } from '../../../features/request/RequestStatus';
import { homeHubSend } from '../../../features/common/api/HomeHub';
import HomeHubSendMethod from '../../../features/common/api/HomeHubSendMethod';

interface EditRequestDialogProps{
  isOpen:boolean
  setOpen(open:boolean):void
  update():void
  request:AdminRequestDto
}
export default function EditRequestDialog(props: EditRequestDialogProps) {
    const {isOpen, setOpen, request, update} = props
    //console.log(request.data)
    const [dataParsed, setDataParsed] = React.useState({} as any)
    const [status, setStatus] = React.useState({} as RequestStatus)
    const [statusReason, setStatusReason] = React.useState("")
    const [readonly, setReadonly] = React.useState(false)
    const [save, setSave] = React.useState(false)

    const theme = useTheme()
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'))
    const [disableActions, setDisableActions] = React.useState(false)

    useEffect (() => {
      if(isOpen){
        setDataParsed(JSON.parse(request.data))
        setStatus(RequestStatus.from(request.status))
        setStatusReason(request.statusReason||"")
        setReadonly(request.status != "Pending")
        setDisableActions(false)
        setSave(false)
      }
    },[isOpen])
    useEffect(()=>{
      if(save){
        homeHubSend(HomeHubSendMethod.ProcessAdminRequests, {
        data:JSON.stringify(dataParsed),
        id:request.id,
        status:status.value,
        statusReason} as ProcessRequestRequest)?.then((response: any)=>{
          if(response?.error!=null)return;
          update()
          handleClose()
        })
        /*adminRequestApi.processRequestAsync({
        data:JSON.stringify(dataParsed),
        id:request.id,
        status:status.value,
        statusReason} as ProcessRequestRequest).then(()=>{
          update()
          handleClose()
        })*/
      }
    }, [save])

    const validate = () =>{
      return true
    }
    const handleClose = () => {
      setOpen(false);
    }
    const handleSave = () =>{
      if(!validate())return;
      setDisableActions(true)
      setSave(true)
    }
    const handleAccept = ()=>{
      setStatus(RequestStatus.Accepted)
      handleSave()
    }
    const handleDeny = ()=>{
      setStatus(RequestStatus.Denied)
      handleSave()
    }
    return (
      <div>
        <Dialog open={isOpen} onClose={handleClose} fullScreen={fullScreen}>
          <DialogTitle>Редактирование заявки {request.id} {readonly&&" (решение принято)"}</DialogTitle>
          <DialogContent>
              <Typography pt={1}>
                <span style={{color: status.color}}>{status.text}</span>
              </Typography>
            { Object.keys(dataParsed).map((key)=>(
              <TextField
                key={key}
                margin="dense"
                label={`Data.${key}`}
                type="text"
                fullWidth
                variant="standard"
                value={dataParsed[key]}
                onChange={(e)=>{
                  let newDataParsed = {...dataParsed, [key]:e.target.value }
                  setDataParsed(newDataParsed)
                }}
                InputProps={{
                  readOnly: readonly,
                }}
              />
            ))}
              <TextField
                autoFocus
                margin="dense"
                label="Причина решения"
                type="text"
                fullWidth
                variant="standard"
                value={""+statusReason}
                onChange={(e)=>{setStatusReason(e.target.value)}}
                InputProps={{
                  readOnly: readonly,
                }}
              />
              <Typography pt={1} variant="caption" display="block">Created {request.createdAt} </Typography>
              <Typography variant="caption" display="block">Updated {request.updatedAt} </Typography>
          </DialogContent>
          <DialogActions>
            <Tooltip title="Закрыть">
              <IconButton onClick={handleClose}>
                <CloseIcon/>
              </IconButton>
            </Tooltip>
            <Tooltip title="Сохранить">
              <span>
              <IconButton onClick={handleSave} disabled={disableActions||readonly}>
                <SaveIcon/>
              </IconButton>
              </span>
            </Tooltip>
            <Tooltip title="Сохранить и принять">
              <span>
              <IconButton onClick={handleAccept} disabled={disableActions||readonly} color="success">
                <AddCircleIcon/>
              </IconButton>
              </span>
            </Tooltip>
            <Tooltip title="Сохранить и отклонить">
              <span>
              <IconButton onClick={handleDeny} disabled={disableActions||readonly} color="error">
                <RemoveCircleIcon/>
              </IconButton>
              </span>
            </Tooltip>
          </DialogActions>
        </Dialog>
      </div>
    );
  }