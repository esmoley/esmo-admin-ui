import { Box, Button, Card, CardActions, CardContent, Checkbox, CircularProgress, Grid, LinearProgress, List, ListItem, ListItemText, Paper, Popover, styled, Table, TableBody, TableCell, TableRow, Tooltip, Typography } from "@mui/material";
import React, { useEffect } from "react";
import { AdminRequestDto } from "../../../features/admin/request/adminRequestDto";
import AppDrawer from "../../../features/appDrawer/AppDrawer";
import { CenterRequestDto } from "../../../features/request/CenterRequestDto";
import adminRequestApi from "../../../features/admin/request/adminRequestApi";
import EditRequestDialog from "./EditRequestDialog";
import { RequestStatus } from "../../../features/request/RequestStatus";
import HomeHubSendMethod from "../../../features/common/api/HomeHubSendMethod";
import { homeHubSend } from "../../../features/common/api/HomeHub";
import GetAdminRequestsVm from "../../../features/admin/request/GetAdminRequestsVm";

const Root = styled('div')`
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  td,
  th {
    border: 1px solid #ddd;
    text-align: left;
    padding: 8px;
  }

  th {
    background-color: #ddd;
  }
`;

function AdminRequests() {
    const [editDialogOpen, setEditDialogOpen] = React.useState(false)
    const [editDialogRequest, setEditDialogRequest] = React.useState({} as AdminRequestDto)
    const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null)
    const [popoverContent, setPopoverContent] = React.useState(<></>)
    //<Typography sx={{ p: 1 }}>Popover content</Typography>

    const handlePopoverOpen = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handlePopoverClose = () => {
        setAnchorEl(null);
    };
    const isPopoverOpen = Boolean(anchorEl);

    /*let editDialogCenterRequest = {} as CenterRequestDto;
    const setEditDialogCenterRequest = (centerRequestDto: CenterRequestDto)=>{
        editDialogCenterRequest = centerRequestDto;
    }*/
    const [loaded, setLoaded] = React.useState(false);

    const [adminRequests, setAdminRequests] = React.useState(Array<AdminRequestDto>());
    useEffect(() => {
        if (!loaded) {
            homeHubSend(HomeHubSendMethod.GetAdminRequests)?.then((response: GetAdminRequestsVm | any)=>{
                setAdminRequests(response.requests != null ? response.requests : Array<AdminRequestDto>())
                setLoaded(true)
            })
            /*adminRequestApi.getRequestsAsync().then(response => {
                setAdminRequests(response.data != null ? response.data : Array<AdminRequestDto>())
                setLoaded(true)
            })*/
        }
    }, [loaded]);

    let requestsContent = <></>
    if (adminRequests.length > 0) {
        const GetRequestDataPresentation = (req: AdminRequestDto) => {
            let dataParsed = JSON.parse(req.data);
            let dataRows = Array<any>()
            for (const k in dataParsed) {
                dataRows.push(<TableRow key={k}>
                    <TableCell>{k}</TableCell>
                    <TableCell>{dataParsed[k]}</TableCell>
                </TableRow>)
            }
            return (
                <>
                    <Grid py={1}>
                        <Typography variant="body1">
                            Данные:
                        </Typography>
                        <Table size="small" aria-label="a dense table">
                            <TableBody>
                                {dataRows}
                            </TableBody>
                        </Table>
                    </Grid>
                </>
            )
        }
        let reqJsx = adminRequests.map((req, index) =>
        {
            let reqStatus = RequestStatus.from(req.status);
            return <Grid item key={index}>
                <Card >
                    <CardContent>
                        {req.user != null ?
                            <><Typography sx={{ fontSize: 14 }} color="text.secondary" display="inline">
                                Заявка от пользователя
                            </Typography>
                                <Typography sx={{ fontSize: 14 }} color={req.user.isAdmin ? "primary.main" : "success.main"} gutterBottom aria-haspopup="true"
                                    onMouseEnter={(event) => {
                                        setPopoverContent(
                                            <List dense={true}>
                                                <ListItem>
                                                    <ListItemText primary={req.user?.id} secondary="userId" />
                                                </ListItem>
                                                <ListItem>
                                                    <ListItemText primary={<Checkbox checked={req.user?.isAdmin} size="small" />} secondary="isAdmin" />
                                                </ListItem>
                                                <ListItem>
                                                    <ListItemText primary={req.user?.userName} secondary="userName" />
                                                </ListItem>
                                                <ListItem>
                                                    <ListItemText primary={req.user?.email} secondary="email" />
                                                </ListItem>
                                                <ListItem>
                                                    <ListItemText primary={req.user?.phoneNumber} secondary="phoneNumber" />
                                                </ListItem>
                                                <ListItem>
                                                    <ListItemText primary={req.user?.currentCenterId} secondary="currentCenterId" />
                                                </ListItem>
                                                <ListItem>
                                                    <ListItemText primary={req.user?.createdAt} secondary="createdAt" />
                                                </ListItem>
                                                <ListItem>
                                                    <ListItemText primary={req.user?.updatedAt} secondary="updatedAt" />
                                                </ListItem>
                                            </List>)
                                        handlePopoverOpen(event)
                                    }}
                                    onMouseLeave={handlePopoverClose} display="inline">
                                    {" "}{req.user.userName}
                                </Typography>
                            </>
                            :
                            <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                                Заявка от пользователя {req.requestedByUserId}
                            </Typography>
                        }
                        <Typography sx={{ mb: 1.5 }} color="text.secondary">
                            {req.id}
                        </Typography>
                        <Typography color={reqStatus.color}>{reqStatus.text}</Typography>
                        <Typography variant="body1">
                            Тип: {req.type}
                        </Typography>
                        {GetRequestDataPresentation(req)}
                        <Typography variant="caption" display="block">Created {req.createdAt}</Typography>
                        <Typography variant="caption" display="block">Updated {req.updatedAt}</Typography>
                    </CardContent>
                    <CardActions>
                        <Button size="small" onClick={() => {
                            setEditDialogRequest(req)
                            setEditDialogOpen(true)
                        }}>Редактировать</Button>
                    </CardActions>
                </Card>
            </Grid>
            }
        )
        requestsContent = <Grid item><Grid container spacing={2}>{reqJsx}</Grid></Grid>
    }
    let content = <Grid container spacing={1} justifyContent={"space-between"}>
        {requestsContent}
        {adminRequests.length == 0 &&
            <Grid item xs={12}>
                <Typography variant="body1" component="p" fontStyle={{ color: "grey" }}>
                    Нет заявок
                </Typography>
            </Grid>
        }
    </Grid>
    return <>
        <Root>
        <AppDrawer title="Admin - Запросы" loaded={loaded}>
            {content}
            <Popover
                id="mouse-over-popover"
                sx={{
                    pointerEvents: 'none',
                }}
                open={isPopoverOpen}
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                onClose={handlePopoverClose}
                disableRestoreFocus
            >
                {popoverContent}
            </Popover>
        </AppDrawer>
        <EditRequestDialog isOpen={editDialogOpen} setOpen={setEditDialogOpen} update={() => setLoaded(false)} request={editDialogRequest} />
        </Root>
    </>
}
export default AdminRequests;